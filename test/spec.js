var moment = require('moment');
var expect = require('expect.js');
var discovery = require('../sources/calculate.js');

describe('Add time with Moment.js', function(){
    it('should return a string', function(){
        var result = discovery();
        expect(result).to.be.a('string');
        expect(result).to.be.ok();
    });
    it('should return the right date', function(){
        var result = discovery();
        expect(result).to.be.equal('1/1/2015');
    });
});
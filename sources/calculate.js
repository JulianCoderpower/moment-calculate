var moment = require('moment');
module.exports = function calculate(){
    var date = new Date('2014/12/31');

    // Add one day without moment.js
    var dateStringWithoutMoment = new Date(date.getTime() + 24 * 60 * 60 * 1000).toLocaleDateString();
    console.log('dateStringWithoutMoment: ', dateStringWithoutMoment);

    // Add one day with moment.js
    var dateStringWithMoment = moment(date).add(1, 'day').format('l');
    console.log('dateStringWithMoment: ', dateStringWithMoment);

    return dateStringWithMoment;
};